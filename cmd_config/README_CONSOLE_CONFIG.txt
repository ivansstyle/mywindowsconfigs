-----------------------------------------------------------------

	            CONSOLE COMMANDS HELP

-----------------------------------------------------------------

subl [filepath/filename] 	: Open Sublime text editor
pych [filepath/filename]	: Open PyCharn IDE 
tobox 				: Change working dir to dropbox folder 
gshell [path]			: Open Git Shell console at path
				      (default: user directory)
kpython [filepath/filename] 	: Run python with a file/directory
chelp				: Show the console help 
 
---------SHORTCUTS---------
CTRL + SHIFT + ALT + Y 	-> CMD
CTRL + SHIFT + ALT + X 	-> GSHELL

Author: Ivans Saponenko | SUPAMONKS STUDIO | 11.07.2016
